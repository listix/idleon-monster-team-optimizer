#!/usr/bin/python3

import copy
import math
import random
import re
import ast

FIGHTING = 0
FORAGING = 1
BREEDING = 2
BLOOMINGLEVEL = 0
BLOOMINGAXE = 1 + 0.06 * BLOOMINGLEVEL
AREASUNLOCKED = 10

BREEDPADDING = 10

class Breed():
    def __init__(self,fightpower,foraging,breeding):
        self.fightpower = fightpower
        self.foraging   = foraging
        self.breeding   = breeding
    def __str__(self):
        total = sum([self.fightpower,self.foraging,self.breeding])
        return "{} {}".format(str(type(self).__name__).ljust(BREEDPADDING,' '),round(total))
    def __repr__(self):
        return self.__str__()
    def calculate_fightpower(self):
        return self.fightpower
    def calculate_foraging(self):
        return self.foraging

class FighterClass(Breed):
    def __init__(self,fightpower):
        super().__init__(fightpower,0,0)

class ForagingClass(Breed):
    def __init__(self,foraging):
        super().__init__(0,foraging,0)
    def calculate_fightpower(self):
        return BLOOMINGAXE * self.calculate_foraging()

class BreederClass(Breed):
    def __init__(self,breeding):
        super().__init__(0,0,breeding)

class SpecialClass(Breed):
    def __init__(self):
        super().__init__(0,0,0)
 
class Fighter(FighterClass):
    pass

class Defender(FighterClass):
    pass

class Forager(ForagingClass):
    pass

class Fleeter(ForagingClass):
    pass

class Breeder(BreederClass):
    pass

class Special(SpecialClass):
    pass

class Mercenary(FighterClass):
    pass
   
class Boomer(FighterClass):
    pass

class Sniper(FighterClass):
    pass

class Amplifier(FighterClass):
    pass

class Tsar(FighterClass):
    pass

class Rattler(FighterClass):
    pass

class Cursory(FighterClass):
    pass

class Fastidious(ForagingClass):
    pass

class Flashy(ForagingClass):
    pass

class Opticular(ForagingClass):
    pass

class Monolithic(ForagingClass):
    pass

class Alchemic(ForagingClass):
    pass

class Badumdum(ForagingClass):
    pass

class Defstone(FighterClass):
    pass

class Targeter(ForagingClass):
    pass

class Looter(ForagingClass):
    pass

class Refiller(ForagingClass):
    pass

class Eggshell(ForagingClass):
    pass

BUILD = 0
FLAGGY = 1
EXP = 2
EXPSPEED = 3
EXPSPEEDBUILD = 4

def getindex(lis, value):
    try:
        index = lis.index(value)
    except ValueError:
        index = -1
    return index



def findcycle(parent1, parent2, start = 0, cycles = [], group = 0):
    index  = start
    while True:
        cycles[index] = group
        index  = getindex(parent1,parent2[index])
        if index == -1:
            print('ERROR Value: ', parent2[index], ' not found.')
            break
        if  start == index or index == -1:
            break

    return cycles
    

def findallcycles(parent1,parent2):
    assert(len(parent1) == len(parent2))
    cycles = ['X'] * len(parent1)
    start = 0
    group = 0
    while getindex(cycles,'X') != -1:
        findcycle(parent1,parent2,start,cycles,group)
        group += 1
        start = getindex(cycles,'X')
    return cycles
    
def crossover(parent1,parent2,cycles):
    child1 = ['X'] * len(parent1)
    child2 = ['X'] * len(parent2)
    for i in range(len(parent1)):
        if cycles[i] % 2 == 0:
            child1[i] = parent1[i]
            child2[i] = parent2[i]
        else:
            child1[i] = parent2[i]
            child2[i] = parent1[i]

    return (child1,child2)            

def mutation(child):
    if random.randint(1,100) > 30:
        index1 = random.randint(0,len(child)-1)
        index2 = random.randint(0,len(child)-1)
        temp = 0
        temp = child[index1]
        child[index1] = child[index2]
        child[index2] = temp
    
    return child

def rungeneration(solutions,preferredcol):
    #we ignore parent3 onwards, they are too weak.    
    parent1 = solutions[0][1]
    parent2 = solutions[1][1]
    
    child1 = BOARD(parent1.teamshelf,parent1.ordering)
    #child1.setboard(copy.deepcopy(board))
    
    child2 = BOARD(parent1.teamshelf,parent1.ordering)
    #child2.setboard(copy.deepcopy(board))
    
    child3 = BOARD(parent1.teamshelf,parent1.ordering)
    #child3.setboard(copy.deepcopy(board))
    
    child4 = BOARD(parent1.teamshelf,parent1.ordering)
    #child4.setboard(copy.deepcopy(board))
    #print(parent1.ordering,parent2.ordering) 
    (child1.ordering,child3.ordering) = crossover(parent1.ordering,parent2.ordering,findallcycles(parent1.ordering,parent2.ordering))
    #print(child1.ordering,child3.ordering)
    #print(parent1.ordering,parent2.ordering) 
    #(child2.cogorder,child4.cogorder) = crossover(parent3.cogorder,parent4.cogorder,findallcycles(parent3.cogorder,parent4.cogorder))
    #Child of parents
    child1.ordering = mutation(child1.ordering)
    child1.arrangeteams()
    #Only a variation of one of the parents
    child2.ordering = mutation(parent1.ordering)
    child2.arrangeteams()
    #Child of the parents
    child3.ordering = mutation(child3.ordering)
    child3.arrangeteams()
    #we introduce randomness into the children
    child4.ordering = mutation(parent2.ordering)
    random.shuffle(child4.ordering)
    child4.arrangeteams()

    return [(child1.calculate(preferredcol),child1),(child2.calculate(preferredcol),child2),(child3.calculate(preferredcol),child3),(child4.calculate(preferredcol), child4)]

def evolve(teams,ordering,preferredcol,ctype = BUILD):
    print('Setting up initial solutions')
    #startord = list(range(len(shelf)))
    #we dont shuffle it here because if we have a best solution so far we want to improve over it.
    #random.shuffle(ordering)
    solution1 = BOARD(teams,copy.deepcopy(ordering))
    #solution1.setboard(copy.deepcopy(board))
    #print(solution1)
    #print(solution1.board)
    solution1.arrangeteams()
    #print((solution1,solution1.calculate()))
    print('Current solution %d Total %d' % (solution1.calculate(preferredcol)[preferredcol],solution1.calculate(preferredcol)[-2]))

    random.shuffle(ordering)
    solution2 = BOARD(teams,copy.deepcopy(ordering))
    #solution2.setboard(copy.deepcopy(board))
    solution2.arrangeteams()
    print(solution2)

    random.shuffle(ordering)
    solution3 = BOARD(teams,copy.deepcopy(ordering))    
    #solution3.setboard(copy.deepcopy(board))
    solution3.arrangeteams()

    random.shuffle(ordering)
    solution4 = BOARD(teams,copy.deepcopy(ordering))
    #solution4.setboard(copy.deepcopy(board))
    solution4.arrangeteams()
    bestsolution = copy.deepcopy(solution1)

    print('Evolving solution...')
    count = 0
    generation = 0
    solutions = [(solution1.calculate(preferredcol),solution1),(solution2.calculate(preferredcol),solution2),(solution3.calculate(preferredcol),solution3),(solution4.calculate(preferredcol),solution4)]
    while count < 100000:
        if generation % 10000 == 0:
            print("Generation: {}".format(generation))
        solutions.sort(key=lambda x: x[0][ctype], reverse=True)
        if solutions[0][0][preferredcol] > bestsolution.calculate(preferredcol)[preferredcol]:
            bestsolution = copy.deepcopy(solutions[0][1])
            count = 0
            print('=======================================')
            print('Better solution found %d Total %d' % (bestsolution.calculate(preferredcol)[preferredcol],bestsolution.calculate(preferredcol)[-2]))
            print(bestsolution)
            print('=======================================')
        solutions = rungeneration(solutions,preferredcol)
        count += 1
        generation += 1
    print('--------Characteristics---------')
    print('Foraging speed Beach Docks %d Total Foraging Speed %d' % (bestsolution.calculate(preferredcol)[preferredcol],bestsolution.calculate(preferredcol)[-2]))
    #print(bestsolution.getsolutionboard())
    print(bestsolution)
    print('Serializing Output. This can replace the current cogshelf in the code to start from this solution.')
    #print([bestsolution.cogshelf[i] for i in bestsolution.cogorder])

def parse_pets_data(string):
    pets = []
    team = []
    m = re.match(r"[^\",0-9a-z{}:[]\\]",string)
    if not re.match(r"[^\",0-9a-z{}:[]\\]",string) :
        pets = ast.literal_eval(string)
        for pet in pets:
            if pet[0] not in ['none']:
                if pet[1] == '0':
                    team.append(Fighter(pet[2]))
                elif pet[1] == '1':
                    team.append(Defender(pet[2]))
                elif pet[1] == '2':
                    team.append(Forager(pet[2]))
                elif pet[1] == '3':
                    team.append(Fleeter(pet[2]))
                elif pet[1] == '4':
                    team.append(Breeder(pet[2]))
                elif pet[1] == '5':
                    team.append(Special(pet[2]))
                elif pet[1] == '6':
                    team.append(Mercenary(pet[2]))
                elif pet[1] == '7':
                    team.append(Boomer(pet[2]))
                elif pet[1] == '8':
                    team.append(Sniper(pet[2]))
                elif pet[1] == '9':
                    team.append(Amplifier(pet[2]))
                elif pet[1] == '10':
                    team.append(Tsar(pet[2]))
                elif pet[1] == '11':
                    team.append(Rattler(pet[2]))
                elif pet[1] == '12':
                    team.append(Cursory(pet[2]))
                elif pet[1] == '13':
                    team.append(Fastidious(pet[2]))
                elif pet[1] == '14':
                    team.append(Flashy(pet[2]))
                elif pet[1] == '15':
                    team.append(Opticular(pet[2]))
                elif pet[1] == '16':
                    team.append(Monolithic(pet[2]))
                elif pet[1] == '17':
                    team.append(Alchemic(pet[2]))
                elif pet[1] == '18':
                    team.append(Badumdum(pet[2]))
                elif pet[1] == '19':
                    team.append(Defstone(pet[2]))
                elif pet[1] == '20':
                    team.append(Targeter(pet[2]))
                elif pet[1] == '21':
                    team.append(Looter(pet[2]))
                elif pet[1] == '22':
                    team.append(Refiller(pet[2]))
                elif pet[1] == '23':
                    team.append(Eggshell(pet[2]))
        #row = ""
        #for idx in range(len(team)):
        #    row += str(team[idx])
        #    if idx % 4 == 3:
        #        print(row)
        #        row = ""
        ##print("Number of items {}".format(len(cogs.keys())))
    else:
        print("Invalid cog data provided")

    return team



def expsort(cog):
    if cog.build > 90:
        result = cog.build
    else:
        result = cog.exp
    return result
def string_escape(s, encoding='utf-8'):
    return (s.encode('latin1')         # To bytes, required by 'unicode-escape'
             .decode('unicode-escape') # Perform the actual octal-escaping decode
             .encode('latin1')         # 1:1 mapping back to bytes
             .decode(encoding))        # Decode original encoding

BOARDPADDING = 20

class BOARD(object):
    def __init__(self,data,ordering,levels=1):
        if data is None:
            self.board = [[[1,1,1] for x in range(4)] for cell in range(levels)]
        else:
            maxareas = min(AREASUNLOCKED,math.ceil(len(data)/4))
            self.board = [['X' for x in range(4)] for cell in range(maxareas)]
        self.teamshelf = data
        self.ordering = ordering

    def __str__(self):
        zone = ['Grasslands',
                'Jungle',
                'Enchroaching Forest',
                'Tree Interior',
                'Stinky Sewers',
                'Desert Oasis',
                'Beach Docks',
                'Coarse Mountains',
                'Twilight Desert',
                'The Crypt',
                'Frosty Peaks',
                'Tundra Outback',
                'Crystal Caverns',
                'Pristalle Lake',
                'Nebulon Mantle',
                'Starfield Skies',
                'Shores of Eternity']
        line ='+'+('-'*(BOARDPADDING+1)+'+')*5+'\n'
        for idx,row in enumerate(self.board):
            for jdx,cell in enumerate(row):
                if jdx == 0:
                   line += "|{} ".format(zone[idx].ljust(BOARDPADDING,' '))
                line += "|{} ".format(str(cell).ljust(BOARDPADDING,' '))
            line +='|\n'+'+'+('-'*(BOARDPADDING+1)+'+')*5+'\n'
        return line

    def setboard(self,board):
        self.board = board

    def arrangeteams(self):
        i = 0
        for idx,row in enumerate(self.board):
            for jdx,cell in enumerate(row):
                if i < len(self.teamshelf):
                    self.board[idx][jdx] = self.teamshelf[self.ordering[i]]
                    i += 1
    
    def create_boost_board(self,levels):
        boostboard = BOARD(None,None,levels)
        return boostboard        
    
    
    def apply_fighter(self,current,row,col):
        pass
    def apply_defender(self,current,row,col):
        pass
    def apply_forager(self,current,row,col):
        self.board[row][col][FORAGING] *= 2  
    def apply_fleeter(self,current,row,col):
        for idx in range(4):
            self.board[row][idx][FORAGING] *= 1.3
    def apply_breeder(self,current,row,col):
        pass
    def apply_special(self,current,row,col):
        pass
    def apply_mercenary(self,current,row,col):
        self.board[row][col][FIGHTING] *= 2  
    def apply_boomer(self,current,row,col):
        pass
    def apply_sniper(self,current,row,col):
        pass
    def apply_amplifier(self,current,row,col):
        pass
    def apply_tsar(self,current,row,col):
        if len(self.board) > row - 1 >= 0:
            for idx in range(4):
                self.board[row-1][idx][FIGHTING] *= 1.5
        
        if len(self.board) > row + 1 >= 0:
            for idx in range(4):
                self.board[row+1][idx][FIGHTING] *= 1.5

    def apply_rattler(self,current,row,col):
        pass
    def apply_cursory(self,current,row,col):
        pass
    def apply_fastidious(self,current,row,col):
        if any([isinstance(pet,FighterClass) for pet in current.board[row]]):
            for idx in range(4):
                self.board[row][idx][FORAGING] *= 1.5

    def apply_flashy(self,current,row,col):
        if not any([isinstance(pet,FighterClass) for pet in current.board[row]]):
            for idx in range(4):
                self.board[row][idx][FORAGING] *= 1.5

    def apply_opticular(self,current,row,col):
        if current.board[row][col].calculate_foraging() > max([current.board[row][idx].calculate_foraging() for idx in range(len(current.board[row])) if idx != col]):
            self.board[row][col][FORAGING] *= 3

    def apply_monolithic(self,current,row,col):
        #add filling the bar
        pass

    def apply_alchemic(self,current,row,col):
        pass
    def apply_badumdum(self,current,row,col):
        if len(self.board) > row - 1 >= 0:
            for idx in range(4):
                self.board[row-1][idx][FORAGING] *= 1.2
        for idx in range(4):
            self.board[row][idx][FORAGING] *= 1.2
        if len(self.board) > row + 1 >= 0:
            for idx in range(4):
                self.board[row+1][idx][FORAGING] *= 1.2

    def apply_defstone(self,current,row,col):
        pass
    def apply_targeter(self,current,row,col):
        if len(self.board) > row - 1 >= 0 and isinstance(current.board[row-1][col],Targeter):
            self.board[row][col][FORAGING] *= 5

    def apply_looter(self,current,row,col):
        #add filling the bar
        pass
    def apply_refiller(self,current,row,col):
        pass
    def apply_eggshell(self,current,row,col):
        pass
   
    def apply_buff(self,row,col,svert,shori,value):
        pass 
    def calculate(self,preferredcol):
        boostboard = self.create_boost_board(len(self.board))
        for idx in range(len(self.board)):
            for jdx in range(len(self.board[idx])):
                if isinstance(self.board[idx][jdx],Fighter):
                    boostboard.apply_fighter(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Defender):
                    boostboard.apply_defender(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Forager):
                    boostboard.apply_forager(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Fleeter):
                    boostboard.apply_fleeter(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Breeder):
                    boostboard.apply_breeder(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Special):
                    boostboard.apply_special(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Mercenary):
                    boostboard.apply_mercenary(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Boomer):
                    boostboard.apply_boomer(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Sniper):
                    boostboard.apply_sniper(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Amplifier):
                    boostboard.apply_amplifier(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Tsar):
                    boostboard.apply_tsar(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Rattler):
                    boostboard.apply_rattler(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Cursory):
                    boostboard.apply_cursory(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Fastidious):
                    boostboard.apply_fastidious(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Flashy):
                    boostboard.apply_flashy(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Opticular):
                    boostboard.apply_opticular(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Monolithic):
                    boostboard.apply_monolithic(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Alchemic):
                    boostboard.apply_alchemic(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Badumdum):
                    boostboard.apply_badumdum(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Defstone):
                    boostboard.apply_defstone(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Targeter):
                    boostboard.apply_targeter(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Looter):
                    boostboard.apply_looter(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Refiller):
                    boostboard.apply_refiller(self,idx,jdx)
                elif isinstance(self.board[idx][jdx],Eggshell):
                    boostboard.apply_eggshell(self,idx,jdx)
                else:
                    pass
                    print('--------- Unknown -----------')
                    print(self.board[idx][jdx])
                    print('--------- Unknown -----------')
                    #raise("Unknown type")
        fight = []
        forage = []
        for idx in range(len(self.board)):
            row_fight_tot = 0
            row_forage_tot = 0
            for jdx in range(len(self.board[idx])):
                row_fight_tot += self.board[idx][jdx].calculate_fightpower()*boostboard.board[idx][jdx][FIGHTING]
                row_forage_tot += self.board[idx][jdx].calculate_foraging()*boostboard.board[idx][jdx][FORAGING]
            fight.append(row_fight_tot)
            forage.append(row_forage_tot)
        return  forage + [sum(forage)] + [forage[preferredcol]*100 + sum(forage)]
#isinstance(contents, LOCK)
        
            
def main(preferredzone=0,evolutiontype = 0):
    #parse the cog data
    pets_data = "" 
    with open('pets.txt','r') as f:
        pets_data = string_escape(f.readline())
    pets = parse_pets_data(pets_data)
    with open('petsstored.txt','r') as f:
        pets_data = string_escape(f.readline())

    pets += parse_pets_data(pets_data)
    teams = pets
             
    ordering = list(range(len(teams)))
    evolve(teams,ordering,preferredzone,evolutiontype)

if __name__=="__main__":
    main(6,-1)
